Instructions
----

1. Implement NumberSorter.java so that NumberSorterTest.java passes.
2. Look at Patient.java and PatientTest.java and make ANY improvements that you would make to your production code, 
nothing is off limits.  There are some tests already written for you in PatientTest.java, you will need to add more.
