package com.socialcare;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumberSearcherTest {

    @Test
    public void testSmallest1() {
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int smallest = NumberSearcher.getSmallestValue(input);
        assertEquals(1, smallest);
    }

    @Test
    public void testSmallest2() {
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 0, 9};
        int smallest = NumberSearcher.getSmallestValue(input);
        assertEquals(0, smallest);
    }

    @Test
    public void testSecondSmallest1() {
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int low2 = NumberSearcher.getSecondSmallestValue(input);
        assertEquals(2, low2);
    }

    @Test
    public void testSecondSmallest2() {
        int[] input = {6, 5, 4, 1, 2, 3, 5, 6, 7};
        int low2 = NumberSearcher.getSecondSmallestValue(input);
        assertEquals(2, low2);
    }

    @Test
    public void testSecondSmallest3() {
        int[] input = {1, 1, 1, 2, 2, 2, 3, 3, 3};
        int low2 = NumberSearcher.getSecondSmallestValue(input);
        assertEquals(2, low2);
    }

    //TODO add custom exception handling:
    // create NumberSearchException class (which extends Exception) with appropriate String constructor.
    // make NumberSearcher.getSecondSmallestValue throw NumberSearchException when given less than 2 distinct values
    // add tests in this file which verify that the correct exception class is thrown

}