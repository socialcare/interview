package com.socialcare;

import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PatientTest {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    @Ignore // TODO: remove ignore and make pass...
    @Test
    public void testBirthdates() throws Exception {
        Patient p1 = new Patient("Foo", sdf.parse("2017/01/01"));
        assertEquals(0, p1.getCount());

        Patient p2 = new Patient("Foo", sdf.parse("2016/01/03"));
        assertEquals(1, p2.getCount());

        Patient p3 = new Patient("Foo", sdf.parse("2015/04/04"));
        assertEquals(1, p3.getCount());

        Patient p4 = new Patient("Foo", sdf.parse("2016/01/29"));
        assertEquals(0, p4.getCount());

        Patient p5 = new Patient("Foo", sdf.parse("2015/12/29"));
        assertEquals(1, p5.getCount());

        Patient p6 = new Patient("Foo", sdf.parse("1917/01/01"));
        assertEquals(100, p6.getCount());
    }

    @Test
    public void testGetName() throws Exception {
        assertTrue(true);
        // TODO: create a patient and implement a test for Patient#getName()
    }
}