package com.socialcare;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class NumberSorterTest {

    @Test
    public void testNumberSorter1() {
        int[] inp = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int low2 = NumberSorter.getSecondSmallestValue(inp);
        assertEquals(2, low2);
    }

    @Test
    public void testNumberSorter2() {
        int[] inp = {6, 5, 4, 1, 2, 3, 5, 6, 7};
        int low2 = NumberSorter.getSecondSmallestValue(inp);
        assertEquals(2, low2);
    }

    @Test
    public void testNumberSorter3() {
        int[] inp = {1, 1, 1, 2, 2, 2, 3, 3, 3};
        int low2 = NumberSorter.getSecondSmallestValue(inp);
        assertEquals(2, low2);
    }

    @Test
    public void testNumberSorter4() {
        try {
            int[] inp = {1};
            int low2 = NumberSorter.getSecondSmallestValue(inp);
            fail();
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), "Not enough numbers.");
            e.printStackTrace();
        }
    }
}