package com.socialcare;

/**
 * Created by llansford on 5/5/17.
 */
public class NumberSearchException extends Exception {

    public NumberSearchException(String message) {
        super(message);
    }
}
