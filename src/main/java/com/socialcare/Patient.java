package com.socialcare;

import java.util.Date;

// TODO: Improve this class as you see fit.
public class Patient {

    private String name;
    private Date dateOfBirth;

    public Patient(String myName, Date d) {
        name = myName;
        dateOfBirth = d;
    }

    public String getName() {
        return name;
    }

    // TODO: is something wrong with this method, does it's test pass?
    public int getCount() throws Exception {
        Date dob = new Date();
        dob.setTime(dateOfBirth.getTime());
        Date jan1 = DateHelper.getDateAtJanuary1st(new Date());

        if (dob.compareTo(jan1) == 0) {
            return 0;
        } else {
            for (int count = 1; count < 1000; count++) {
                dob = DateHelper.addOneYear(dob);
                if (jan1.compareTo(dob) == 0) {
                    return count;
                }
            }
            throw new RuntimeException("Person too old? Loop did no terminate");
        }
    }
}
