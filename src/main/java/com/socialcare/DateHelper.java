package com.socialcare;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

    public static Date getDateAtJanuary1st(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.WEEK_OF_YEAR, 1);
        calendar.set(Calendar.WEEK_OF_MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date addOneYear(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.YEAR, 1);
        return cal.getTime();
    }

    //if you pass in:
    //d1 = January 14, 2000
    //d2 = April 10, 2017
    //we ignore the month and day component, and compare 2000 to 2017, so return 17;
    //
    //if you pass in:
    //d1 = December 1, 2016
    //d2 = January 14, 2017
    //we ignore the month and day component, and compare 2016 to 2017, so return 1;
    public static int getDifferenceInYears(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        return cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
    }

    //if you pass in:
    //d1 = January 14, 2000
    //d2 = April 10, 2017
    //we ignore the year component, and compare January 14 to April 10, so return -1;
    //
    //if you pass in:
    //d1 = April 14, 2015
    //d2 = April 14, 2017
    //we ignore the year component, and return 0 (equality);
    public static int compareDatesDayAndMonth(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);

        Integer cal1Month = cal1.get(Calendar.MONTH);
        Integer cal2Month = cal2.get(Calendar.MONTH);

        //compare months first
        int monthComparison = cal1Month.compareTo(cal2Month);
        if (monthComparison != 0) {
            //dates are in different months
            return monthComparison;
        }
        //the dates have the same month, we need to look at the DAY_OF_MONTH
        Integer cal1Day = cal1.get(Calendar.DAY_OF_MONTH);
        Integer cal2Day = cal2.get(Calendar.DAY_OF_MONTH);
        return cal1Day.compareTo(cal2Day);
    }

    public LocalDate createLocalDate(Date d) {
        LocalDate dob = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        //LocalDate dob = LocalDate.of(d.getYear() + 1900, d.getMonth() + 1, d.getDate());
        return dob;
    }

}